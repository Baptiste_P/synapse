﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Métier;

    [Serializable()]
    public class Mission
    {
        private string _nom;
        private string _description ;
        private int _nbHeuresPrevues ;
        private Dictionary<DateTime, int> _releveHoraire ;
        private Intervenant _executant ;

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public int NbHeuresPrevues
        {
            get { return _nbHeuresPrevues; }
            set { _nbHeuresPrevues = value; }
        }

        public Dictionary<DateTime,int> ReleveHoraire
            {
            get { return _releveHoraire; }
            set { _releveHoraire = value; }
            }

        public Intervenant Executant 
        {
            get { return _executant; }
            set { _executant = value; }
        }

    public int NbHeuresEffectues()
    {
        int heuresReelles = 0;
        foreach (KeyValuePair<DateTime, int> kvp in _releveHoraire)
        {

            heuresReelles += kvp.Value;
        }
        return heuresReelles;

    }

    public void AjoutReleve(DateTime date, int nbHeures)
    {
        this._releveHoraire.Add(date,nbHeures);

        //_releveHoraires = type dictionary 
    }
    

  


}     


