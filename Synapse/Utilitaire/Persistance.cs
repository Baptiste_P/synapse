﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using System.Collections;
using Synapse.Métier;

namespace Synapse.Utilitaire
{
     public static class Persistance
    {
        private static string repertoireApplication = Environment.CurrentDirectory + @"\";

        /// <summary>
        /// Charge une collection stockée dans un fichier de sauvegarde
        /// </summary>
        /// <param name="nomFichier">Le nom du fichier de sauvegarde de la collection</param>
        /// <returns>Une collection</returns>
        
        public static List<Projet> ChargerProjet ()
        {
            FileStream fs = null;
            List<Projet> ListeProjet = null; 

            try
            {
                fs = new FileStream(repertoireApplication +"", FileMode.Open);
                BinaryFormatter formatter = new BinaryFormatter(); 
                try
                {
                    ListeProjet = (List<Projet>)formatter.Deserialize(fs);
                }
                catch (SerializationException err)
                {
                    Persistance.FichierLog(err);

                }
                finally
                {
                    fs.Close();
                }

            }
            catch(Exception erreur)
            {

            }
            return ListeProjet;
        }
        /// <summary>
        /// Sauvegarde une collection dans un fichier de sauvegarde
        /// </summary>
        /// <param name="nomFichier">Le nom du fichier de sauvegarde</param>
        /// <param name="collection">La collection sauvegardée</param>
        /// 

        public static void SauvegarderProjet(List<Projet> collectionProjet )
        {
            FileStream file = null;

            try
            {
                file = File.Open(repertoireApplication + "", FileMode.OpenOrCreate);
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(file, collectionProjet);
            }
            catch (Exception err)
            {
                Persistance.FichierLog(err);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }

        }


        public static List<Intervenant> ChargerIntervenant()
        {
            FileStream fs = null;
            List<Intervenant> ListeIntervenant = null;

            try
            {
                fs = new FileStream(repertoireApplication + "", FileMode.Open);
                BinaryFormatter formatter = new BinaryFormatter();
                try
                {
                    ListeIntervenant= (List<Intervenant>)formatter.Deserialize(fs);
                }
                catch (SerializationException err)
                {
                    Persistance.FichierLog(err);

                }
                finally
                {
                    fs.Close();
                }

            }
            catch (Exception erreur)
            {

            }
            return ListeIntervenant;
        }

        public static void SauvegarderIntervenant(List<Intervenant> collectionIntervenant)
        {
            FileStream file = null;

            try
            {
                file = File.Open(repertoireApplication + "", FileMode.OpenOrCreate);
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(file, collectionIntervenant);
            }
            catch (Exception err)
            {
                Persistance.FichierLog(err);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }

        }


        private static void FichierLog(Exception exception)
        {
            string path = repertoireApplication + @"\erreur";
            try
            {
                StreamWriter sw = new StreamWriter(path, true);
                sw.WriteLine("Date : " + DateTime.Now + " Message : " + exception.Message);
                sw.Close();
            }
            catch (IOException IOExcep)
            {
                throw new NotImplementedException();
            }
        }
    }
}
